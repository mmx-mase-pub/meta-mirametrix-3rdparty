DESCRIPTION = "The sdk for FLIR cameras"
AUTHOR = "FLIR"
HOMEPAGE = "https://www.flir.fr/products/flycapture-sdk/"
LICENSE = "Proprietary"
LIC_FILES_CHKSUM = "file://${WORKDIR}/LICENSE.pdf;md5=ee4e7ddb004ae3366c248d97a0db2ae4"

SRC_URI += " \
        file://LICENSE.pdf \
        file://flycapture.${PV}_${TARGET_ARCH}.tar.gz \
"

def archive_root_folder_name(d):
    import re
    arch = d.getVar('TARGET_ARCH', True)
    if   re.match('i.86$', arch):    return 'flycapture2-2.13.3.31-i386'
    elif re.match('x86_64$', arch):  return 'flycapture2-2.13.3.31-amd64'
    elif re.match('arm$', arch): return 'flycapture.2.13.3.31_armhf'
    elif re.match('aarch64$', arch): return 'flycapture.2.13.3.31_arm64'

# FLIR package naming convention differs depending on the arch.
#
S = "${WORKDIR}/${@archive_root_folder_name(d)}"

RDEPENDS_${PN} = " \
        libusb1 \
        libswscale \
        libavcodec \
        libavutil \
        libavformat \
"

RDEPENDS_${PN}_append_x86 = "libraw1394 bash"
RDEPENDS_${PN}_append_x86-64 = "libraw1394 bash ${PN}-linuxloader"

do_install_x86() {
        do_install_debian_package
}

do_install_x86-64() {
        do_install_debian_package
        do_install_lib64
}

do_install_arm() {
        do_install_arm_package
}

do_install_aarch64() {
        do_install_arm_package
}

do_install_debian_package() {
        # Unpack the .deb archives.
        for file in $(find -name '*.deb'); do
                ar vx "${file}"
                tar xvf ${S}/data.tar.xz --no-same-owner
        done

        # Install the shared libraries.
        # libflycapturegui.so is omitted for now.
        for name in \
                libflycapture \
                libflycapture-c \
                libflycapturevideo \
                libflycapturevideo-c \
                libmultisync \
                libmultisync-c
        do
                # Install the .so
                install -Dm 0755 "usr/lib/${name}.so.${PV}" "${D}${libdir}/$(basename ${name}).so.${PV}"

                # Copy the symlinks
                cp -d "usr/lib/${name}.so.2" "${D}${libdir}"
                cp -d "usr/lib/${name}.so" "${D}${libdir}"
        done

        # Install the test applications
        for file in \
                AsyncTriggerEx \
                BusEventsEx \
                CustomImageEx \
                ExtendedShutterEx \
                flycap \
                FlyCapture2Test \
                FlyCapture2Test_C \
                GigEConfigCmd \
                GigEGrabEx \
                GigEGrabEx_C \
                GrabCallbackEx \
                HighDynamicRangeEx \
                ImageEventEx \
                MultipleCameraEx \
                multisync2 \
                MultiSyncEx \
                MultiSyncEx_C \
                SaveImageToAviEx \
                SaveImageToFlashEx \
                UpdatorCLI2
        do
                install -Dm 0755 "usr/bin/${file}" "${D}${bindir}/$(basename ${file})"
        done

        # Install the includes
        for file in $(find usr/include -type f -printf "%P\n"); do
                install -Dm 0644 "usr/include/${file}" "${D}${includedir}/${file}"
        done
}

# for get_linuxloader()
inherit linuxloader

do_install_lib64() {
        dynamic_loader=${@get_linuxloader(d)}
        dynamic_loader_basename=$(basename ${dynamic_loader})
        if [ ${dynamic_loader_basename} = "ld-linux-x86-64.so.2" ]; then
                install -d ${D}/lib64
                lnr ${D}${dynamic_loader} ${D}/lib64/${dynamic_loader_basename}
        else
                bberror "Cannot install flycapture2 prebuilt executables because the rootfs built doesn't use the same ELF loader."
        fi
}

do_install_arm_package() {
        # Install the shared libraries.
        # libflycapturegui.so is omitted for now.
        for name in \
                libflycapture \
                libflycapturevideo \
                C/libflycapture-c \
                C/libflycapturevideo-c
        do
                # Install the .so and .a
                install -Dm 0755 "lib/${name}.so.${PV}" "${D}${libdir}/$(basename ${name}).so.${PV}"
                install -Dm 0644 "lib/${name}_static.a" "${D}${libdir}/$(basename ${name})_static.a"

                # Copy the symlinks
                cp -d "lib/${name}.so.2" "${D}${libdir}"
                cp -d "lib/${name}.so" "${D}${libdir}"
        done

        # Install the test applications
        for file in \
                AsyncTriggerEx \
                BusEventsEx \
                C/FlyCapture2Test_C \
                CustomImageEx \
                ExtendedShutterEx \
                FlyCapture2Test \
                GigEGrabEx \
                GrabCallbackEx \
                HighDynamicRangeEx \
                ImageEventEx \
                MultipleCameraEx
        do
                install -Dm 0755 "bin/${file}" "${D}${bindir}/$(basename ${file})"
        done

        # Install the includes
        for file in $(find ./include -type f -printf "%P\n"); do
                install -Dm 0644 "./include/${file}" "${D}${includedir}/flycapture/${file}"
        done
}

INSANE_SKIP_${PN} += "already-stripped"
FILES_${PN}-dev = "${includedir} /usr/src/ ${libdir}/*.so"
FILES_${PN}-staticdev = "${libdir}/*.a"
FILES_${PN} = "${libdir}/*.so.* ${bindir}"

# The flycapture2-linuxloader contains a single symlink to the real linuxloader.
# It is only required for x86-64.
#
FILES_${PN}-linuxloader = "/lib64/ld-linux-x86-64.so.2"
INSANE_SKIP_${PN}-linuxloader += "libdir"
PACKAGES += " ${PN}-linuxloader"
