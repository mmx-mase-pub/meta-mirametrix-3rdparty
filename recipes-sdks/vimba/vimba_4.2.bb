DESCRIPTION = "The sdk for allied vision cameras"
AUTHOR = "Allied Vision"
HOMEPAGE = "https://www.alliedvision.com/en/products/software.html"

require vimba.inc

SRC_URI_append_aarch64 = " \
        file://${PV}/Vimba_aarch64.tgz \
"

SRC_URI_append_x86-64 = " \
        file://${PV}/Vimba_x86_64.tgz \
"
