DESCRIPTION = "The sdk for allied vision cameras"
AUTHOR = "Allied Vision"
HOMEPAGE = "https://www.alliedvision.com/en/products/software.html"

require vimba.inc

SRC_URI_append = " \
        file://${PV}/Vimba_i_86.tgz \
"
