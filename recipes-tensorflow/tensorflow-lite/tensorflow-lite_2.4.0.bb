DESCRIPTION = "TensorFlow’s lightweight solution for mobile and embedded devices"
AUTHOR = "Google Inc. and Yuan Tang"
HOMEPAGE = "https://www.tensorflow.org/lite"
LICENSE = "Apache-2.0"

require tensorflow.inc

SRC_URI += " \
	file://0001-add-yocto-toolchain-to-support-cross-compiling.patch \
	file://0001-fix-build-tensorflow-lite-examples-label_image-label.patch \
	file://0001-label_image-tweak-default-model-location.patch \
	file://0001-label_image.lite-tweak-default-model-location.patch \
	file://0001-CheckFeatureOrDie-use-warning-to-avoid-die.patch \
	file://0001-support-32-bit-x64-and-arm-for-yocto.patch \
	file://BUILD.in \
	file://BUILD.yocto_compiler \
	file://cc_config.bzl.tpl \
	file://yocto_compiler_configure.bzl \
"

do_configure_append () {
	mkdir -p ${S}/third_party/toolchains/yocto/
	sed "s#%%CPU%%#${BAZEL_TARGET_CPU}#g" ${WORKDIR}/BUILD.in  > ${S}/third_party/toolchains/yocto/BUILD
	chmod 644 ${S}/third_party/toolchains/yocto/BUILD
	install -m 644 ${WORKDIR}/cc_config.bzl.tpl ${S}/third_party/toolchains/yocto/
	install -m 644 ${WORKDIR}/yocto_compiler_configure.bzl ${S}/third_party/toolchains/yocto/
	install -m 644 ${WORKDIR}/BUILD.yocto_compiler ${S}

	CT_NAME=$(echo ${HOST_PREFIX} | rev | cut -c 2- | rev)
	SED_COMMAND="s#%%CT_NAME%%#${CT_NAME}#g"
	SED_COMMAND="${SED_COMMAND}; s#%%WORKDIR%%#${WORKDIR}#g"
	SED_COMMAND="${SED_COMMAND}; s#%%YOCTO_COMPILER_PATH%%#${BAZEL_OUTPUTBASE_DIR}/external/yocto_compiler#g"

	sed -i "${SED_COMMAND}" ${S}/BUILD.yocto_compiler \
		${S}/WORKSPACE

	${TF_CONFIG} \
	./configure
}

TF_TARGET_EXTRA ??= ""
do_compile() {
        export CT_NAME=$(echo ${HOST_PREFIX} | rev | cut -c 2- | rev)
        unset CC
        ${BAZEL} build \
        ${TF_ARGS_EXTRA} \
        -c opt \
        --cpu=${BAZEL_TARGET_CPU} \
        --subcommands --explain=${T}/explain.log \
        --verbose_explanations --verbose_failures \
        --crosstool_top=@local_config_yocto_compiler//:toolchain \
        --host_crosstool_top=@bazel_tools//tools/cpp:toolchain \
        --verbose_failures \
        --copt -DTF_LITE_DISABLE_X86_NEON \
        //tensorflow/lite/c:libtensorflowlite_c.so \
        //tensorflow/lite:libtensorflowlite.so \
        ${TF_TARGET_EXTRA}
}

do_install() {
        install -d ${D}${libdir}
        install -m 644 ${S}/bazel-bin/tensorflow/lite/c/libtensorflowlite_c.so \
        ${D}${libdir}

        install -m 644 ${S}/bazel-bin/tensorflow/lite/libtensorflowlite.so \
        ${D}${libdir}

        cd "${S}/tensorflow/lite"
        for file in $(find . -name '*.h'); do
                install -D -m 0644 "${file}" "${D}${includedir}/tensorflow/lite/${file}"
        done
}

inherit unsupportarch

INSANE_SKIP_${PN} += "already-stripped"
FILES_${PN}-dev = "${includedir}/*"
FILES_${PN} = "${libdir}/*"
