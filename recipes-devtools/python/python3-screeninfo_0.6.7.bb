SUMMARY = ""
HOMEPAGE = "https://github.com/rr-/screeninfo"

LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://README.md;md5=d77aff778d5bcfe6ea59c85f3175a1c4"

SRC_URI[md5sum] = "8903253cdad634af471d1942ce1e1d3d"
SRC_URI[sha256sum] = "1c4bac1ca329da3f68cbc4d2fbc92256aa9bb8ff8583ee3e14f91f0a7baa69cb"

PYPI_PACKAGE = "screeninfo"

inherit pypi setuptools3

BBCLASSEXTEND = "native nativesdk"
