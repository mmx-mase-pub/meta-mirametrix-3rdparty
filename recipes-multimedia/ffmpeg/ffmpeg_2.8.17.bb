require ffmpeg.inc

SRC_URI = "https://www.ffmpeg.org/releases/${BP}.tar.xz \
           file://0001-custom-version.patch \
           "
SRC_URI[md5sum] = "75e4d737bb6ea211718a5246fdbeb85b"
SRC_URI[sha256sum] = "d0734fec613fe12bee0b5a84f917779b854c1ede7882793f618490e6bbf0c148"

EXTRA_FFCONF = "--build-suffix='-ffmpeg'"

FILES_libavcodec = "${libdir}/libavcodec-ffmpeg${SOLIBS}"
FILES_libavdevice = "${libdir}/libavdevice-ffmpeg${SOLIBS}"
FILES_libavfilter = "${libdir}/libavfilter-ffmpeg${SOLIBS}"
FILES_libavformat = "${libdir}/libavformat-ffmpeg${SOLIBS}"
FILES_libavresample = "${libdir}/libavresample-ffmpeg${SOLIBS}"
FILES_libavutil = "${libdir}/libavutil-ffmpeg${SOLIBS}"
FILES_libpostproc = "${libdir}/libpostproc-ffmpeg${SOLIBS}"
FILES_libswresample = "${libdir}/libswresample-ffmpeg${SOLIBS}"
FILES_libswscale = "${libdir}/libswscale-ffmpeg${SOLIBS}"
