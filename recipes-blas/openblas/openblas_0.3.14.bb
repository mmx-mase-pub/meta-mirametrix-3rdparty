DESCRIPTION = "OpenBLAS is an optimized BLAS library based on GotoBLAS2 1.13 BSD version."
SUMMARY = "OpenBLAS : An optimized BLAS library"
HOMEPAGE = "http://www.openblas.net/"
LICENSE = "BSD"
LIC_FILES_CHKSUM = "file://LICENSE;md5=5adf4792c949a00013ce25d476a2abc0"

SRC_URI = "\
        git://github.com/xianyi/OpenBLAS;branch=develop;rev=v${PV};protocol=https \
	"

S = "${WORKDIR}/git"

def map_arch(d):
    import re
    arch = d.getVar('TARGET_ARCH', True)
    if   re.match('i.86$', arch):    return 'ATOM'
    elif re.match('x86_64$', arch):  return 'ATOM'
    elif re.match('aarch32$', arch): return 'CORTEXA9'
    elif re.match('aarch64$', arch): return 'ARMV8'
    return 'CORTEXA15'

def map_bits(d):
    import re
    arch = d.getVar('TARGET_ARCH', True)
    if   re.match('i.86$', arch):    return 32
    elif re.match('x86_64$', arch):  return 64
    elif re.match('aarch32$', arch): return 32
    elif re.match('aarch64$', arch): return 64
    return 32

EXTRA_OEMAKE = "\
    ONLY_CBLAS=1 \
    HOSTCC=${BUILD_CC} \
    CROSS=1 \
    CROSS_SUFFIX=${TARGET_PREFIX} \
    BINARY=${@map_bits(d)} \
    TARGET=${@map_arch(d)} \
"

do_install() {
    oe_runmake PREFIX=${prefix} DESTDIR=${D} install
    rm -rf ${D}${bindir} ${D}${libdir}/cmake
}

FILES_${PN}-dev = "${includedir} ${libdir}/lib${PN}.so"
FILES_${PN}     = "${libdir}/*"
